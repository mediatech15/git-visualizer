#### [v1.8.2](https://gitlab.com/mediatech15/git-visualizer/compare/v1.8.2...v1.8.2)

#### [v1.8.2](https://gitlab.com/mediatech15/git-visualizer/compare/v1.8.1...v1.8.2)

> 18 May 2023

**Changes**

- credits update [`f016938`](https://gitlab.com/mediatech15/git-visualizer/commit/f0169380e692800aae8b0db8cec37086b48a3358)
- changlog updates [`0cc6f7e`](https://gitlab.com/mediatech15/git-visualizer/commit/0cc6f7e7d3ccffab6da94cfb61bfeff401a4e3f0)

#### [v1.8.1](https://gitlab.com/mediatech15/git-visualizer/compare/v1.8.0...v1.8.1)

> 18 May 2023

**Changes**

- ci updates [`596ec7b`](https://gitlab.com/mediatech15/git-visualizer/commit/596ec7bedddceb802908585081a2c530342ffdd3)

#### [v1.8.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.7.0...v1.8.0)

> 18 May 2023

**Changes**

- update dependencies and readme, fix arg parse [`4d78c5f`](https://gitlab.com/mediatech15/git-visualizer/commit/4d78c5fbe765f327774e8cb448861df9d7769819)
- updated to use yargs [`d7fc511`](https://gitlab.com/mediatech15/git-visualizer/commit/d7fc5116c0e2943fe41e6ed3a61d9532796c7d1f)
- Update README.md [`ec8e065`](https://gitlab.com/mediatech15/git-visualizer/commit/ec8e0652d487e83c058dc74e5576ce513eb66321)
- Reduce inlined data [`35e318a`](https://gitlab.com/mediatech15/git-visualizer/commit/35e318a22675e9547527f5c944dc0c201e2f1c11)

#### [v1.7.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.6.0...v1.7.0)

> 31 October 2022

**Changes**

- fix package.json reference for package distro [`9311c2c`](https://gitlab.com/mediatech15/git-visualizer/commit/9311c2c8788a30ee9068ac6f21a7f21252998642)

#### [v1.6.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.5.0...v1.6.0)

> 31 October 2022

**Changes**

- fix package.json reference for package distro [`96d1baa`](https://gitlab.com/mediatech15/git-visualizer/commit/96d1baa7bf962892f6b6c324feb4abf9b9cd9495)

#### [v1.5.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.4.0...v1.5.0)

> 31 October 2022

**Changes**

- fix Ci for publish [`19679d4`](https://gitlab.com/mediatech15/git-visualizer/commit/19679d481af67393b13fdad16bd02e6246ab39b4)
- Update CHANGELOG.md [`bbe8f01`](https://gitlab.com/mediatech15/git-visualizer/commit/bbe8f01879968d9687984d2fab012a9a8334b4c8)

#### [v1.4.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.2.0...v1.4.0)

> 31 October 2022

**Changes**

- fix ci update version [`fb9e353`](https://gitlab.com/mediatech15/git-visualizer/commit/fb9e353f251392d6b4c5acb052445989d52c0a2f)
- fix ci [`97a0194`](https://gitlab.com/mediatech15/git-visualizer/commit/97a0194882b9a3957dd50ba4b8f7f8b167cf8711)
- added basic arg parse [`ec26252`](https://gitlab.com/mediatech15/git-visualizer/commit/ec26252a5f61f045db476cd7dd39cc82516a55a4)
- update readme for new prompt [`1149319`](https://gitlab.com/mediatech15/git-visualizer/commit/11493190e13436bcd1e376b3f0fa3225a4e727d1)
- update folder validation, add timing, add open after [`13020c6`](https://gitlab.com/mediatech15/git-visualizer/commit/13020c6c3a791e923db8898226de34256d03a2a7)
- added progress indication [`0d14285`](https://gitlab.com/mediatech15/git-visualizer/commit/0d142859f3a7f896fc70870b1013bc26fd1f0b03)
- update readme credits [`d2329ed`](https://gitlab.com/mediatech15/git-visualizer/commit/d2329ed302b4056dd8ac54ee45b3282e15c6bf72)
- fix dependencies and ci [`d17809f`](https://gitlab.com/mediatech15/git-visualizer/commit/d17809f43d7187aae6bca4c63dba119be69983e0)
- update file pathing [`35c6c3d`](https://gitlab.com/mediatech15/git-visualizer/commit/35c6c3d08efce1755beff2f7424fde20b85c7dd4)
- update ci and readme [`5a8bcc0`](https://gitlab.com/mediatech15/git-visualizer/commit/5a8bcc0d32cf76bd520868993eab06d98755b57d)
- update ci and add changelog [`acf48db`](https://gitlab.com/mediatech15/git-visualizer/commit/acf48dbc8fe2e2d4731d5111b5d4f777e86c5c7d)
- update ci [`21fbaef`](https://gitlab.com/mediatech15/git-visualizer/commit/21fbaefda9986e94f8d4cb97ab9eb9b41996bbce)
- update ci [`7fbb98a`](https://gitlab.com/mediatech15/git-visualizer/commit/7fbb98a97b50cd16a3f9cff03dd7a15d99ff69a2)
- update ci [`19fec64`](https://gitlab.com/mediatech15/git-visualizer/commit/19fec64ac4f07a2aaa0b919b5437bf425d65687b)
- update ci [`bf998a7`](https://gitlab.com/mediatech15/git-visualizer/commit/bf998a7cfda5ffca13a25427bf38ea39b437b1f8)
- update ci [`962e2f1`](https://gitlab.com/mediatech15/git-visualizer/commit/962e2f17c58555f9aa885ff52c0b385fb03527e7)
- update ci [`50f69b0`](https://gitlab.com/mediatech15/git-visualizer/commit/50f69b0853b5b0762450dc979d1010de095329ea)
- update ci [`a4503c9`](https://gitlab.com/mediatech15/git-visualizer/commit/a4503c9ec4c14551035f2dd0ab96cb3204509d3d)
- update ci [`1f7767a`](https://gitlab.com/mediatech15/git-visualizer/commit/1f7767abc4b607c075bc1d903e2c19616e027cc8)
- update ci [`52ed229`](https://gitlab.com/mediatech15/git-visualizer/commit/52ed229ce66b9fcf357e23213b528ded882658f5)
- Add LICENSE [`b75cf15`](https://gitlab.com/mediatech15/git-visualizer/commit/b75cf1571c0acf01519746fea362ee16090ccd9c)
- update ci [`0ca373b`](https://gitlab.com/mediatech15/git-visualizer/commit/0ca373bd53a06c41b220a2cf557a8c49c68da880)
- update ci [`b64a76d`](https://gitlab.com/mediatech15/git-visualizer/commit/b64a76d455a49291e2585f980ee56694b6937809)
- add release [`44a7b43`](https://gitlab.com/mediatech15/git-visualizer/commit/44a7b43cb68b3335e9adf816e8c9f223e5f92aaf)

#### [v1.2.0](https://gitlab.com/mediatech15/git-visualizer/compare/v1.1.0...v1.2.0)

> 29 October 2022

**Changes**

- update node ref [`633fd8f`](https://gitlab.com/mediatech15/git-visualizer/commit/633fd8fa43b571cdfa301c3feedb4c4d05dc2cb3)

#### v1.1.0

> 29 October 2022

**Changes**

- working proto [`f9e56a0`](https://gitlab.com/mediatech15/git-visualizer/commit/f9e56a023d9bd8843e4bbb6f74fcbc0a6b301c77)
- working proto [`0882927`](https://gitlab.com/mediatech15/git-visualizer/commit/08829274263146961f699a860c4a68b19c71cc3e)
- init [`906603c`](https://gitlab.com/mediatech15/git-visualizer/commit/906603c3b8db5e2f0b4a0887ca87dad3e9611075)
- Initial commit [`648f928`](https://gitlab.com/mediatech15/git-visualizer/commit/648f92868a195c903245b74429af9a3c6d3f16f6)
