#!/usr/bin/env node
process.env.NODE_ENV = "production";
import { main, run } from './index.js';
import { fileURLToPath } from 'url';
import { basename, dirname } from 'path';
import yargs from 'yargs';
const argv = yargs(process.argv.slice(2)).argv
delete argv._
delete argv.$0

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);
const base = basename(__dirname);

if (Object.keys(argv).length > 0) {
	let answers = {
		confirm: true,
		openAfter: false,
		repoPath: __dirname,
		commits: -1,
		outputFile: `${base}-all_commits-visualized`,
		outputPath: __dirname
	};
	if (argv.default){
		run(answers)
	} else {
		if (argv.openAfter) {
			answers.openAfter = Boolean(argv.openAfter);
		} 
		if (argv.repoPath) {
			if (argv.repoPath != true) {
				answers.repoPath = argv.repoPath;
			}
		}
		if (argv.commits) {
			answers.commits = Int(argv.commits);
			let com = Int(argv.commits);
			if (com == -1) {
				com = 'all';
			}
			answers.outputFile = `${basename(answers.repoPath)}-${com}_commits-visualized`;
		}
		if (argv.outputFile) {
			answers.outputFile = argv.outputFile;
		}
		if (argv.outputPath) {
			answers.outputPath = argv.outputPath;
		}
		run(answers);
	}
} else {
	main();
}
