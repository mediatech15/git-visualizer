import inquirer from 'inquirer';
import { basename, join, dirname, normalize } from 'path';
import inquirerFileTreeSelection from 'inquirer-file-tree-selection-prompt';
import { getHomeFolder } from 'platform-folders';
import c from 'cli-color';
import { existsSync, readFileSync, writeFileSync } from 'fs';
import { fileURLToPath } from 'url';
import { header, postComplete } from './constants.js';
import { generateJson, setLoggingPrefix } from 'node-git-2-json';
import open from 'open';
import updateNotifier from 'update-notifier';
import packageJson from '../package.json' assert {type: 'json'};


const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

let startEventTime = null;
let lastEventTime = null;

const notifier = updateNotifier(
	{
		pkg: packageJson,
		updateCheckInterval: 0,
		shouldNotifyInNpmScript: true,
		distTag: "latest",
	}
)

console.log(header);

export async function main() {
	inquirer.registerPrompt('file-tree-selection', inquirerFileTreeSelection);
	inquirer.prompt(
		[
			{
				type: 'file-tree-selection',
				name: 'repoPath',
				message: 'Path to the repository?',
				onlyShowDir: true,
				root: __dirname,
				enableGoUpperDirectory: true,
				prefix: '>',
				hideChildrenOfValid: true,
				validate: (item) => {
					if (existsSync(join(item, '.git'))) {
						return true;
					}
					return false;
				}
			},
			{
				type: 'number',
				name: 'commits',
				message: 'How many commits to show. -1 = All?',
				default: -1,
				prefix: '>'
			},
			{
				type: 'input',
				name: 'outputFile',
				message: 'Output file name (.html auto appended)?',
				default: (answers) => {
					let com = answers.commits;
					if (com == -1) {
						com = 'all';
					}
					return `${basename(answers.repoPath)}-${com}_commits-visualized`;
				},
				prefix: '>'
			},
			{
				type: 'file-tree-selection',
				name: 'outputPath',
				message: 'Where would you like the output file placed?',
				onlyShowDir: true,
				root: getHomeFolder(),
				enableGoUpperDirectory: true,
				prefix: '>'
			},
			{
				type: 'confirm',
				name: 'openAfter',
				message: 'Open file after creation',
				prefix: '>'
			},
			{
				type: 'confirm',
				name: 'confirm',
				message: (answers) => {
					let com = answers.commits;
					if (com == -1) {
						com = 'all';
					}
					let o = 'will not';
					if (answers.openAfter) {
						o = 'will';
					}
					return `We will be making a git visualization with ${c.blueBright.underline(answers.repoPath)} consisting of ${c.magentaBright(com)} commits. This will be saved to ${c.blueBright.underline(answers.outputPath + '\\' + answers.outputFile + '.html')} and we ${c.magentaBright(o)} be opening it after.\n\nConfirm?`;
				},
				prefix: '\n>'
			}
		]
	).then(async (answers) => {
		await run(answers);
	});
}

export async function run(answers) {
	console.log();
	lastEventTime = new Date();
	startEventTime = new Date();
	if (answers.confirm) {
		setLoggingPrefix('Git2Json: ');
		console.log(`Starting creation`);
		let data = await getJson(answers);
		let file = await makeGraph(data);
		await exportFile(file, answers);
		if (answers.openAfter) {
			console.log(`Opening outputted file ${getMilliStamp()}`);
			open(normalize(join(answers.outputPath, answers.outputFile + '.html')));
		}
	} else {
		console.log(`Confirmation was ${c.cyan('No')}. Exiting. ${getMilliStamp()}`);
	}
	console.log(`Total time to complete ${getMilliTotal()}`);
	console.log();
	notifier.notify()
	console.log(postComplete);
}

async function getJson(answers) {
	console.log(`Processing repository ${getMilliStamp()}`);
	let j = await generateJson(normalize(answers.repoPath), answers.commits, 'Object', {
		keys: [
			'body',
			'notes'
		],
		sanitize: 'light'
	});
	let j2 = j;
	for (let item in j) {
		let mergedRef = '';
		if (j[item]['refs'].length > 0) {
			mergedRef = j[item]['refs'].join(' | ');
			j2[item]['refs'] = [
				mergedRef
			];
		}
		j2[item]['stats'] = [];
	}
	console.log(`Processing complete ${getMilliStamp()}`);
	return j2;
}

async function makeGraph(data) {
	console.log(`Adding data into page ${getMilliStamp()}`);
	let file = readFileSync(normalize(join(__dirname, 'index.html'))).toString();
	file = file.replace('%% GRAPH_DATA %%', JSON.stringify(data, null, 2));
	return file;
}

async function exportFile(file, answers) {
	console.log(`Adding rendering code into page ${getMilliStamp()}`);
	let gg = readFileSync(normalize(join(__dirname, 'gitgraph.umd.js')));
	file = file.replace('%% GITGRAPH_JS %%', gg);
	let p = normalize(join(answers.outputPath, answers.outputFile + '.html'));
	console.log(`Writing final file to ${c.blueBright.underline(p)} ${getMilliStamp()}`);
	writeFileSync(p, file);
}

function getMilliStamp() {
	let val = c.green.italic(`+${new Date() - lastEventTime}ms`);
	lastEventTime = new Date();
	return val;
}

function getMilliTotal() {
	return c.green.italic(`${new Date() - startEventTime}ms`);
}