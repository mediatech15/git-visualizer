import c from 'cli-color';
import { readFileSync } from 'fs';
import { dedent } from 'ts-dedent';
import { fileURLToPath } from 'url';
import { dirname, join } from 'path';

const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

const info = JSON.parse(readFileSync(join(__dirname, '..', 'package.json'), 'utf8'));


export const appName = 'Git Visualizer';
export const appDescription = info.description;

export const header = dedent(`
	${c.whiteBright.bold(appName)}
	${c.whiteBright('Version:')} ${c.magenta(info.version)}

	${c.green(appDescription)}

`);

export const postComplete = dedent(`
	${c.magenta('Thanks for using')} ${c.bold.italic.cyan(appName)}

	${c.white('Problems and featuers can be requested here')} ${c.blue.underline(info.bugs.url)}

`);