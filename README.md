# git-visualizer-cli

`git-visualizer-cli` is a tool that exports static html pages that leverage `@gitgraph/js` to render a map or graph of the repository and changes. The resulting pages are truely static and self contained. They can be used offline and the outputted file can be passed around without needing other files or internet for cdn downloads.

## Installation

`npm install -g git-visualizer-cli`

## Usage

Installing the package globally allows you to invoke it anywhere. You can install it locally to a project or a non-global area but you will have to be in that local area and invoke via `npx` 

If invoking via general cmd or powershell there are 3 commands available (they all perform the same actions)

- `gitv`
- `git-visualizer`
- `git-visualizer-cli`

The tool will prompt you with a series of questions. The questions will gather the required information to generate the map. The questions are

- What is the repository path
	- This it the base folder for the repository it contains the `.git` folder
- How many commits to consider for the graph
	- This includes the last `n` commits from all branches
	- to show all commits use the default or pass `-1`
- Output file name
	- The default name is the `[REPO_FOLDER_NAME]-[# OF COMMITS]_commits-visualized`
	- The extension `.html` is auto appended
	- `-1` for commits is translated to `all`
	- Example: `my-awesome-repo-all_commits-visualized`
- Where you would like the file placed
	- This is the folder you would like the html file placed
- Open file after creation
	- Should the tool lauch the default tool to view the file.
- Confirm
	- This will state the previous responses in a statement to confirm that what you would like to happen is ok

After answering the questions the tool will create and output the file in directory requested and close.

## Advanced Usage 

The tool has a primative arg parser to map values to questions. The args all have defaults listed below. *Any unexpected args will cause an error and exit*

|arg|type/values|example|description|
|---|---|---|---|
|`--openAfter`||`--openAfter`|Open exported file after|
|`--no-openAfter`||`--no-openAfter`|Don't open file after|
|`--repoPath`|`string<path>`|`--repoPath "C:\Project"`|Base path of the repo|
|`--commits`|`int =-1 or >=1`|`--commits 1000`|Number of commits to use for tree|
|`--outputFile`|`string<filename>`|`--outputFile "My Awesome File"`|Name of exported file|
|`--outputPath`|`string<path>`|`--outputPath "C:\Project"`|Directory to place file|
|`--default`||`--default`|Use defaults for all above options and skip interactive mode|

### Defaults for options

- openAfter `false`
- repoPath `cwd()`
- commits `-1`
- outputFile `${basename(cwd())}-all_commits-visualized`
- outputPath `cwd()`

*Passing `--default` will ignore all other arguements and immediately run with defaults. This is an easy way to bypass interactive mode.*

*Note that by passing in arguements that the `confirm` answer is implied and passed as `yes|true`*

## Notes

- We modify data returned by `node-git-2-json`
	- We remove items we dont need to inline smaller amount of data
	- We concat multiple refs since `@gitgraph/js` doesn't support multiple refs at te same commit
- Large repositories 
	- can take some time to generate
	- can take some time to load in a browser
	- can produce large HTML files
- `@gitgraph/js` is inlined to the output file
	- this allows for portability
	- this allows for offline use
	- `@gitgraph/js` is released under a MIT license (same as this one)
- The raw data to be parsed for gitgraph is inlined
	- the rendering happens in browser

## Future features

- Static HTML rendering
	- no need for inlined data
	- no need for inlined `@gitgraph/js`
- Conversions to other formats
	- pdf
	- png
	- jpg
	- tiff
	- svg

## Credits

- @gitgraph (rendering library)
- node-git-2-json (git to json conversion)
	- inspired by @fabien0102/git2json
- inquirer (cli prompting)
- inquirer-file-tree-selection-prompt (cli file/directory browser)
- platform-folders (cross-platform folder access)
- cli-color (terminal formatting/coloring)
- ts-dedent (multiline string formatting)
- open (open final file)
- yargs (args parsing)
- auto-changelog (Changelog generation)
- update-notifier (update checking)
- ux-license-report (3rd party license report)

`git-visualizer-cli` is released under the MIT license
